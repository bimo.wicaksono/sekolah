
        /**
        * Generated from flexurio at Mon Jul  8 11:17:30 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './guru.html';

    Template.guru.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA GURU');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('guru', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.guru.onRendered(function () {
          ScrollHandler();
      });

      Template.guru.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        gurus: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodeguru: { $regex : new RegExp(textSearch, 'i') }},
       
       {namaguru: { $regex : new RegExp(textSearch, 'i') }},
       
       {jeniskelamin: { $regex : new RegExp(textSearch, 'i') }},
       
       {agama: { $regex : new RegExp(textSearch, 'i') }},
       
       {tempatlahir: { $regex : new RegExp(textSearch, 'i') }},
       
       {tanggallahir: { $regex : new RegExp(textSearch, 'i') }},
       
       {alamat: { $regex : new RegExp(textSearch, 'i') }},
       
       {notlp: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return GURU.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.guru.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deleteGURU();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaGURU);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_guru').modal('show')
       },
       'keyup #namaGURU': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updateGURU(tpl);
               } else {
                   insertGURU(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updateGURU(tpl);
            } else {
                insertGURU(tpl);
            }
          $('#modal_guru').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodeguruGURU').value = this.kodeguru;
       
       document.getElementById('namaguruGURU').value = this.namaguru;
       
       document.getElementById('jeniskelaminGURU').value = this.jeniskelamin;
       
       document.getElementById('agamaGURU').value = this.agama;
       
       document.getElementById('tempatlahirGURU').value = this.tempatlahir;
       
       document.getElementById('tanggallahirGURU').value = this.tanggallahir;
       
       document.getElementById('alamatGURU').value = this.alamat;
       
       document.getElementById('notlpGURU').value = this.notlp;
       
          Session.set('idEditing', this._id);
          $('#modal_guru').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, GURU);
        },

    });


    insertGURU = function (tpl) {

       
       let kodeguruGURU = tpl.$('input[name="kodeguruGURU"]').val();
       
       let namaguruGURU = tpl.$('input[name="namaguruGURU"]').val();
       
       let jeniskelaminGURU = tpl.$('input[name="jeniskelaminGURU"]').val();
       
       let agamaGURU = tpl.$('input[name="agamaGURU"]').val();
       
       let tempatlahirGURU = tpl.$('input[name="tempatlahirGURU"]').val();
       
       let tanggallahirGURU = tpl.$('input[name="tanggallahirGURU"]').val();
       
       let alamatGURU = tpl.$('input[name="alamatGURU"]').val();
       
       let notlpGURU = tpl.$('input[name="notlpGURU"]').val();
       

       if(!adaDATA(kodeguruGURU) | !adaDATA(namaguruGURU) | !adaDATA(jeniskelaminGURU) | !adaDATA(agamaGURU) | !adaDATA(tempatlahirGURU) | !adaDATA(tanggallahirGURU) | !adaDATA(alamatGURU) | !adaDATA(notlpGURU) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       GURU.insert(
       {
          
       kodeguru: kodeguruGURU,
       
       namaguru: namaguruGURU,
       
       jeniskelamin: jeniskelaminGURU,
       
       agama: agamaGURU,
       
       tempatlahir: tempatlahirGURU,
       
       tanggallahir: tanggallahirGURU,
       
       alamat: alamatGURU,
       
       notlp: notlpGURU,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updateGURU = function (tpl) {

        
       let kodeguruGURU = tpl.$('input[name="kodeguruGURU"]').val();
       
       let namaguruGURU = tpl.$('input[name="namaguruGURU"]').val();
       
       let jeniskelaminGURU = tpl.$('input[name="jeniskelaminGURU"]').val();
       
       let agamaGURU = tpl.$('input[name="agamaGURU"]').val();
       
       let tempatlahirGURU = tpl.$('input[name="tempatlahirGURU"]').val();
       
       let tanggallahirGURU = tpl.$('input[name="tanggallahirGURU"]').val();
       
       let alamatGURU = tpl.$('input[name="alamatGURU"]').val();
       
       let notlpGURU = tpl.$('input[name="notlpGURU"]').val();
       

       if(!adaDATA(kodeguruGURU) | !adaDATA(namaguruGURU) | !adaDATA(jeniskelaminGURU) | !adaDATA(agamaGURU) | !adaDATA(tempatlahirGURU) | !adaDATA(tanggallahirGURU) | !adaDATA(alamatGURU) | !adaDATA(notlpGURU) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       GURU.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodeguru: kodeguruGURU,
       
       namaguru: namaguruGURU,
       
       jeniskelamin: jeniskelaminGURU,
       
       agama: agamaGURU,
       
       tempatlahir: tempatlahirGURU,
       
       tanggallahir: tanggallahirGURU,
       
       alamat: alamatGURU,
       
       notlp: notlpGURU,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deleteGURU = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    GURU.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
