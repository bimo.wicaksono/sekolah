
        /**
        * Generated from flexurio at Mon Jul  8 09:39:32 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './kelas.html';

    Template.kelas.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA KELAS');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('kelas', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.kelas.onRendered(function () {
          ScrollHandler();
      });

      Template.kelas.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        kelass: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodekelas: { $regex : new RegExp(textSearch, 'i') }},
       
       {namakelas: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return KELAS.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.kelas.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deleteKELAS();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaKELAS);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_kelas').modal('show')
       },
       'keyup #namaKELAS': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updateKELAS(tpl);
               } else {
                   insertKELAS(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updateKELAS(tpl);
            } else {
                insertKELAS(tpl);
            }
          $('#modal_kelas').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodekelasKELAS').value = this.kodekelas;
       
       document.getElementById('namakelasKELAS').value = this.namakelas;
       
          Session.set('idEditing', this._id);
          $('#modal_kelas').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, KELAS);
        },

    });


    insertKELAS = function (tpl) {

       
       let kodekelasKELAS = tpl.$('input[name="kodekelasKELAS"]').val();
       
       let namakelasKELAS = tpl.$('input[name="namakelasKELAS"]').val();
       

       if(!adaDATA(kodekelasKELAS) | !adaDATA(namakelasKELAS) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       KELAS.insert(
       {
          
       kodekelas: kodekelasKELAS,
       
       namakelas: namakelasKELAS,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updateKELAS = function (tpl) {

        
       let kodekelasKELAS = tpl.$('input[name="kodekelasKELAS"]').val();
       
       let namakelasKELAS = tpl.$('input[name="namakelasKELAS"]').val();
       

       if(!adaDATA(kodekelasKELAS) | !adaDATA(namakelasKELAS) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       KELAS.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodekelas: kodekelasKELAS,
       
       namakelas: namakelasKELAS,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deleteKELAS = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    KELAS.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
