
        /**
        * Generated from flexurio at Mon Jul  8 09:43:53 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './mapel.html';

    Template.mapel.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA MAPEL');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('mapel', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.mapel.onRendered(function () {
          ScrollHandler();
      });

      Template.mapel.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        mapels: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodemapel: { $regex : new RegExp(textSearch, 'i') }},
       
       {namamapel: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return MAPEL.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.mapel.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deleteMAPEL();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaMAPEL);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_mapel').modal('show')
       },
       'keyup #namaMAPEL': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updateMAPEL(tpl);
               } else {
                   insertMAPEL(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updateMAPEL(tpl);
            } else {
                insertMAPEL(tpl);
            }
          $('#modal_mapel').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodemapelMAPEL').value = this.kodemapel;
       
       document.getElementById('namamapelMAPEL').value = this.namamapel;
       
          Session.set('idEditing', this._id);
          $('#modal_mapel').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, MAPEL);
        },

    });


    insertMAPEL = function (tpl) {

       
       let kodemapelMAPEL = tpl.$('input[name="kodemapelMAPEL"]').val();
       
       let namamapelMAPEL = tpl.$('input[name="namamapelMAPEL"]').val();
       

       if(!adaDATA(kodemapelMAPEL) | !adaDATA(namamapelMAPEL) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       MAPEL.insert(
       {
          
       kodemapel: kodemapelMAPEL,
       
       namamapel: namamapelMAPEL,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updateMAPEL = function (tpl) {

        
       let kodemapelMAPEL = tpl.$('input[name="kodemapelMAPEL"]').val();
       
       let namamapelMAPEL = tpl.$('input[name="namamapelMAPEL"]').val();
       

       if(!adaDATA(kodemapelMAPEL) | !adaDATA(namamapelMAPEL) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       MAPEL.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodemapel: kodemapelMAPEL,
       
       namamapel: namamapelMAPEL,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deleteMAPEL = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    MAPEL.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
