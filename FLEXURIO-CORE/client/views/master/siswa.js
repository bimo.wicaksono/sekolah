
        /**
        * Generated from flexurio at Mon Jul  8 11:02:34 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './siswa.html';

    Template.siswa.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA SISWA');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('siswa', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.siswa.onRendered(function () {
          ScrollHandler();
      });

      Template.siswa.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        siswas: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodesiswa: { $regex : new RegExp(textSearch, 'i') }},
       
       {namasiswa: { $regex : new RegExp(textSearch, 'i') }},
       
       {jeniskelamin: { $regex : new RegExp(textSearch, 'i') }},
       
       {agama: { $regex : new RegExp(textSearch, 'i') }},
       
       {tempatlahir: { $regex : new RegExp(textSearch, 'i') }},
       
       {tanggallahir: { $regex : new RegExp(textSearch, 'i') }},
       
       {alamat: { $regex : new RegExp(textSearch, 'i') }},
       
       {notlp: { $regex : new RegExp(textSearch, 'i') }},
       
       {status: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodekelas1: { $regex : new RegExp(textSearch, 'i') }},
       
       {periode: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodekelas2: { $regex : new RegExp(textSearch, 'i') }},
       
       {periode2: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodekelas3: { $regex : new RegExp(textSearch, 'i') }},
       
       {periode3: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return SISWA.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.siswa.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deleteSISWA();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaSISWA);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_siswa').modal('show')
       },
       'keyup #namaSISWA': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updateSISWA(tpl);
               } else {
                   insertSISWA(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updateSISWA(tpl);
            } else {
                insertSISWA(tpl);
            }
          $('#modal_siswa').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodesiswaSISWA').value = this.kodesiswa;
       
       document.getElementById('namasiswaSISWA').value = this.namasiswa;
       
       document.getElementById('jeniskelaminSISWA').value = this.jeniskelamin;
       
       document.getElementById('agamaSISWA').value = this.agama;
       
       document.getElementById('tempatlahirSISWA').value = this.tempatlahir;
       
       document.getElementById('tanggallahirSISWA').value = this.tanggallahir;
       
       document.getElementById('alamatSISWA').value = this.alamat;
       
       document.getElementById('notlpSISWA').value = this.notlp;
       
       document.getElementById('statusSISWA').value = this.status;
       
       document.getElementById('kodekelas1SISWA').value = this.kodekelas1;
       
       document.getElementById('periodeSISWA').value = this.periode;
       
       document.getElementById('kodekelas2SISWA').value = this.kodekelas2;
       
       document.getElementById('periode2SISWA').value = this.periode2;
       
       document.getElementById('kodekelas3SISWA').value = this.kodekelas3;
       
       document.getElementById('periode3SISWA').value = this.periode3;
       
          Session.set('idEditing', this._id);
          $('#modal_siswa').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, SISWA);
        },

    });


    insertSISWA = function (tpl) {

       
       let kodesiswaSISWA = tpl.$('input[name="kodesiswaSISWA"]').val();
       
       let namasiswaSISWA = tpl.$('input[name="namasiswaSISWA"]').val();
       
       let jeniskelaminSISWA = tpl.$('input[name="jeniskelaminSISWA"]').val();
       
       let agamaSISWA = tpl.$('input[name="agamaSISWA"]').val();
       
       let tempatlahirSISWA = tpl.$('input[name="tempatlahirSISWA"]').val();
       
       let tanggallahirSISWA = tpl.$('input[name="tanggallahirSISWA"]').val();
       
       let alamatSISWA = tpl.$('input[name="alamatSISWA"]').val();
       
       let notlpSISWA = tpl.$('input[name="notlpSISWA"]').val();
       
       let statusSISWA = tpl.$('input[name="statusSISWA"]').val();
       
       let kodekelas1SISWA = tpl.$('input[name="kodekelas1SISWA"]').val();
       
       let periodeSISWA = tpl.$('input[name="periodeSISWA"]').val();
       
       let kodekelas2SISWA = tpl.$('input[name="kodekelas2SISWA"]').val();
       
       let periode2SISWA = tpl.$('input[name="periode2SISWA"]').val();
       
       let kodekelas3SISWA = tpl.$('input[name="kodekelas3SISWA"]').val();
       
       let periode3SISWA = tpl.$('input[name="periode3SISWA"]').val();
       

       if(!adaDATA(kodesiswaSISWA) | !adaDATA(namasiswaSISWA) | !adaDATA(jeniskelaminSISWA) | !adaDATA(agamaSISWA) | !adaDATA(tempatlahirSISWA) | !adaDATA(tanggallahirSISWA) | !adaDATA(alamatSISWA) | !adaDATA(notlpSISWA) | !adaDATA(statusSISWA) | !adaDATA(kodekelas1SISWA) | !adaDATA(periodeSISWA) | !adaDATA(kodekelas2SISWA) | !adaDATA(periode2SISWA) | !adaDATA(kodekelas3SISWA) | !adaDATA(periode3SISWA) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       SISWA.insert(
       {
          
       kodesiswa: kodesiswaSISWA,
       
       namasiswa: namasiswaSISWA,
       
       jeniskelamin: jeniskelaminSISWA,
       
       agama: agamaSISWA,
       
       tempatlahir: tempatlahirSISWA,
       
       tanggallahir: tanggallahirSISWA,
       
       alamat: alamatSISWA,
       
       notlp: notlpSISWA,
       
       status: statusSISWA,
       
       kodekelas1: kodekelas1SISWA,
       
       periode: periodeSISWA,
       
       kodekelas2: kodekelas2SISWA,
       
       periode2: periode2SISWA,
       
       kodekelas3: kodekelas3SISWA,
       
       periode3: periode3SISWA,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updateSISWA = function (tpl) {

        
       let kodesiswaSISWA = tpl.$('input[name="kodesiswaSISWA"]').val();
       
       let namasiswaSISWA = tpl.$('input[name="namasiswaSISWA"]').val();
       
       let jeniskelaminSISWA = tpl.$('input[name="jeniskelaminSISWA"]').val();
       
       let agamaSISWA = tpl.$('input[name="agamaSISWA"]').val();
       
       let tempatlahirSISWA = tpl.$('input[name="tempatlahirSISWA"]').val();
       
       let tanggallahirSISWA = tpl.$('input[name="tanggallahirSISWA"]').val();
       
       let alamatSISWA = tpl.$('input[name="alamatSISWA"]').val();
       
       let notlpSISWA = tpl.$('input[name="notlpSISWA"]').val();
       
       let statusSISWA = tpl.$('input[name="statusSISWA"]').val();
       
       let kodekelas1SISWA = tpl.$('input[name="kodekelas1SISWA"]').val();
       
       let periodeSISWA = tpl.$('input[name="periodeSISWA"]').val();
       
       let kodekelas2SISWA = tpl.$('input[name="kodekelas2SISWA"]').val();
       
       let periode2SISWA = tpl.$('input[name="periode2SISWA"]').val();
       
       let kodekelas3SISWA = tpl.$('input[name="kodekelas3SISWA"]').val();
       
       let periode3SISWA = tpl.$('input[name="periode3SISWA"]').val();
       

       if(!adaDATA(kodesiswaSISWA) | !adaDATA(namasiswaSISWA) | !adaDATA(jeniskelaminSISWA) | !adaDATA(agamaSISWA) | !adaDATA(tempatlahirSISWA) | !adaDATA(tanggallahirSISWA) | !adaDATA(alamatSISWA) | !adaDATA(notlpSISWA) | !adaDATA(statusSISWA) | !adaDATA(kodekelas1SISWA) | !adaDATA(periodeSISWA) | !adaDATA(kodekelas2SISWA) | !adaDATA(periode2SISWA) | !adaDATA(kodekelas3SISWA) | !adaDATA(periode3SISWA) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       SISWA.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodesiswa: kodesiswaSISWA,
       
       namasiswa: namasiswaSISWA,
       
       jeniskelamin: jeniskelaminSISWA,
       
       agama: agamaSISWA,
       
       tempatlahir: tempatlahirSISWA,
       
       tanggallahir: tanggallahirSISWA,
       
       alamat: alamatSISWA,
       
       notlp: notlpSISWA,
       
       status: statusSISWA,
       
       kodekelas1: kodekelas1SISWA,
       
       periode: periodeSISWA,
       
       kodekelas2: kodekelas2SISWA,
       
       periode2: periode2SISWA,
       
       kodekelas3: kodekelas3SISWA,
       
       periode3: periode3SISWA,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deleteSISWA = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    SISWA.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
