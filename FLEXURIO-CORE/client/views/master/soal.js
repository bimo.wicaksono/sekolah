
        /**
        * Generated from flexurio at Mon Jul  8 10:51:01 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './soal.html';

    Template.soal.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA SOAL');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('soal', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.soal.onRendered(function () {
          ScrollHandler();
      });

      Template.soal.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        soals: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodesoal: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodekelas: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodemapel: { $regex : new RegExp(textSearch, 'i') }},
       
       {semester: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodeguru: { $regex : new RegExp(textSearch, 'i') }},
       
       {pertanyaan: { $regex : new RegExp(textSearch, 'i') }},
       
       {pilihan_a: { $regex : new RegExp(textSearch, 'i') }},
       
       {pilihan_b: { $regex : new RegExp(textSearch, 'i') }},
       
       {pilihan_c: { $regex : new RegExp(textSearch, 'i') }},
       
       {pilihan_d: { $regex : new RegExp(textSearch, 'i') }},
       
       {pilihan_e: { $regex : new RegExp(textSearch, 'i') }},
       
       {kuncijawaban: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return SOAL.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.soal.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deleteSOAL();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaSOAL);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_soal').modal('show')
       },
       'keyup #namaSOAL': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updateSOAL(tpl);
               } else {
                   insertSOAL(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updateSOAL(tpl);
            } else {
                insertSOAL(tpl);
            }
          $('#modal_soal').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodesoalSOAL').value = this.kodesoal;
       
       document.getElementById('kodekelasSOAL').value = this.kodekelas;
       
       document.getElementById('kodemapelSOAL').value = this.kodemapel;
       
       document.getElementById('semesterSOAL').value = this.semester;
       
       document.getElementById('kodeguruSOAL').value = this.kodeguru;
       
       document.getElementById('pertanyaanSOAL').value = this.pertanyaan;
       
       document.getElementById('pilihan_aSOAL').value = this.pilihan_a;
       
       document.getElementById('pilihan_bSOAL').value = this.pilihan_b;
       
       document.getElementById('pilihan_cSOAL').value = this.pilihan_c;
       
       document.getElementById('pilihan_dSOAL').value = this.pilihan_d;
       
       document.getElementById('pilihan_eSOAL').value = this.pilihan_e;
       
       document.getElementById('kuncijawabanSOAL').value = this.kuncijawaban;
       
          Session.set('idEditing', this._id);
          $('#modal_soal').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, SOAL);
        },

    });


    insertSOAL = function (tpl) {

       
       let kodesoalSOAL = tpl.$('input[name="kodesoalSOAL"]').val();
       
       let kodekelasSOAL = tpl.$('input[name="kodekelasSOAL"]').val();
       
       let kodemapelSOAL = tpl.$('input[name="kodemapelSOAL"]').val();
       
       let semesterSOAL = tpl.$('input[name="semesterSOAL"]').val();
       
       let kodeguruSOAL = tpl.$('input[name="kodeguruSOAL"]').val();
       
       let pertanyaanSOAL = tpl.$('input[name="pertanyaanSOAL"]').val();
       
       let pilihan_aSOAL = tpl.$('input[name="pilihan_aSOAL"]').val();
       
       let pilihan_bSOAL = tpl.$('input[name="pilihan_bSOAL"]').val();
       
       let pilihan_cSOAL = tpl.$('input[name="pilihan_cSOAL"]').val();
       
       let pilihan_dSOAL = tpl.$('input[name="pilihan_dSOAL"]').val();
       
       let pilihan_eSOAL = tpl.$('input[name="pilihan_eSOAL"]').val();
       
       let kuncijawabanSOAL = tpl.$('input[name="kuncijawabanSOAL"]').val();
       

       if(!adaDATA(kodesoalSOAL) | !adaDATA(kodekelasSOAL) | !adaDATA(kodemapelSOAL) | !adaDATA(semesterSOAL) | !adaDATA(kodeguruSOAL) | !adaDATA(pertanyaanSOAL) | !adaDATA(pilihan_aSOAL) | !adaDATA(pilihan_bSOAL) | !adaDATA(pilihan_cSOAL) | !adaDATA(pilihan_dSOAL) | !adaDATA(pilihan_eSOAL) | !adaDATA(kuncijawabanSOAL) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       SOAL.insert(
       {
          
       kodesoal: kodesoalSOAL,
       
       kodekelas: kodekelasSOAL,
       
       kodemapel: kodemapelSOAL,
       
       semester: semesterSOAL,
       
       kodeguru: kodeguruSOAL,
       
       pertanyaan: pertanyaanSOAL,
       
       pilihan_a: pilihan_aSOAL,
       
       pilihan_b: pilihan_bSOAL,
       
       pilihan_c: pilihan_cSOAL,
       
       pilihan_d: pilihan_dSOAL,
       
       pilihan_e: pilihan_eSOAL,
       
       kuncijawaban: kuncijawabanSOAL,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updateSOAL = function (tpl) {

        
       let kodesoalSOAL = tpl.$('input[name="kodesoalSOAL"]').val();
       
       let kodekelasSOAL = tpl.$('input[name="kodekelasSOAL"]').val();
       
       let kodemapelSOAL = tpl.$('input[name="kodemapelSOAL"]').val();
       
       let semesterSOAL = tpl.$('input[name="semesterSOAL"]').val();
       
       let kodeguruSOAL = tpl.$('input[name="kodeguruSOAL"]').val();
       
       let pertanyaanSOAL = tpl.$('input[name="pertanyaanSOAL"]').val();
       
       let pilihan_aSOAL = tpl.$('input[name="pilihan_aSOAL"]').val();
       
       let pilihan_bSOAL = tpl.$('input[name="pilihan_bSOAL"]').val();
       
       let pilihan_cSOAL = tpl.$('input[name="pilihan_cSOAL"]').val();
       
       let pilihan_dSOAL = tpl.$('input[name="pilihan_dSOAL"]').val();
       
       let pilihan_eSOAL = tpl.$('input[name="pilihan_eSOAL"]').val();
       
       let kuncijawabanSOAL = tpl.$('input[name="kuncijawabanSOAL"]').val();
       

       if(!adaDATA(kodesoalSOAL) | !adaDATA(kodekelasSOAL) | !adaDATA(kodemapelSOAL) | !adaDATA(semesterSOAL) | !adaDATA(kodeguruSOAL) | !adaDATA(pertanyaanSOAL) | !adaDATA(pilihan_aSOAL) | !adaDATA(pilihan_bSOAL) | !adaDATA(pilihan_cSOAL) | !adaDATA(pilihan_dSOAL) | !adaDATA(pilihan_eSOAL) | !adaDATA(kuncijawabanSOAL) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       SOAL.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodesoal: kodesoalSOAL,
       
       kodekelas: kodekelasSOAL,
       
       kodemapel: kodemapelSOAL,
       
       semester: semesterSOAL,
       
       kodeguru: kodeguruSOAL,
       
       pertanyaan: pertanyaanSOAL,
       
       pilihan_a: pilihan_aSOAL,
       
       pilihan_b: pilihan_bSOAL,
       
       pilihan_c: pilihan_cSOAL,
       
       pilihan_d: pilihan_dSOAL,
       
       pilihan_e: pilihan_eSOAL,
       
       kuncijawaban: kuncijawabanSOAL,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deleteSOAL = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    SOAL.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
