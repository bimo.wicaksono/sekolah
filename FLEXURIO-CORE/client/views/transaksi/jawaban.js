
        /**
        * Generated from flexurio at Mon Jul  8 11:40:20 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './jawaban.html';

    Template.jawaban.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA JAWABAN');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('jawaban', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.jawaban.onRendered(function () {
          ScrollHandler();
      });

      Template.jawaban.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        jawabans: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodesoal: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodesiswa: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodeujian: { $regex : new RegExp(textSearch, 'i') }},
       
       {jawaban: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return JAWABAN.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.jawaban.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deleteJAWABAN();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaJAWABAN);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_jawaban').modal('show')
       },
       'keyup #namaJAWABAN': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updateJAWABAN(tpl);
               } else {
                   insertJAWABAN(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updateJAWABAN(tpl);
            } else {
                insertJAWABAN(tpl);
            }
          $('#modal_jawaban').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodesoalJAWABAN').value = this.kodesoal;
       
       document.getElementById('kodesiswaJAWABAN').value = this.kodesiswa;
       
       document.getElementById('kodeujianJAWABAN').value = this.kodeujian;
       
       document.getElementById('jawabanJAWABAN').value = this.jawaban;
       
          Session.set('idEditing', this._id);
          $('#modal_jawaban').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, JAWABAN);
        },

    });


    insertJAWABAN = function (tpl) {

       
       let kodesoalJAWABAN = tpl.$('input[name="kodesoalJAWABAN"]').val();
       
       let kodesiswaJAWABAN = tpl.$('input[name="kodesiswaJAWABAN"]').val();
       
       let kodeujianJAWABAN = tpl.$('input[name="kodeujianJAWABAN"]').val();
       
       let jawabanJAWABAN = tpl.$('input[name="jawabanJAWABAN"]').val();
       

       if(!adaDATA(kodesoalJAWABAN) | !adaDATA(kodesiswaJAWABAN) | !adaDATA(kodeujianJAWABAN) | !adaDATA(jawabanJAWABAN) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       JAWABAN.insert(
       {
          
       kodesoal: kodesoalJAWABAN,
       
       kodesiswa: kodesiswaJAWABAN,
       
       kodeujian: kodeujianJAWABAN,
       
       jawaban: jawabanJAWABAN,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updateJAWABAN = function (tpl) {

        
       let kodesoalJAWABAN = tpl.$('input[name="kodesoalJAWABAN"]').val();
       
       let kodesiswaJAWABAN = tpl.$('input[name="kodesiswaJAWABAN"]').val();
       
       let kodeujianJAWABAN = tpl.$('input[name="kodeujianJAWABAN"]').val();
       
       let jawabanJAWABAN = tpl.$('input[name="jawabanJAWABAN"]').val();
       

       if(!adaDATA(kodesoalJAWABAN) | !adaDATA(kodesiswaJAWABAN) | !adaDATA(kodeujianJAWABAN) | !adaDATA(jawabanJAWABAN) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       JAWABAN.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodesoal: kodesoalJAWABAN,
       
       kodesiswa: kodesiswaJAWABAN,
       
       kodeujian: kodeujianJAWABAN,
       
       jawaban: jawabanJAWABAN,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deleteJAWABAN = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    JAWABAN.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
