
        /**
        * Generated from flexurio at Mon Jul  8 11:38:46 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './peserta.html';

    Template.peserta.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA PESERTA');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('peserta', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.peserta.onRendered(function () {
          ScrollHandler();
      });

      Template.peserta.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        pesertas: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodeujian: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodesiswa: { $regex : new RegExp(textSearch, 'i') }},
       
       {status: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return PESERTA.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.peserta.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deletePESERTA();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaPESERTA);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_peserta').modal('show')
       },
       'keyup #namaPESERTA': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updatePESERTA(tpl);
               } else {
                   insertPESERTA(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updatePESERTA(tpl);
            } else {
                insertPESERTA(tpl);
            }
          $('#modal_peserta').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodeujianPESERTA').value = this.kodeujian;
       
       document.getElementById('kodesiswaPESERTA').value = this.kodesiswa;
       
       document.getElementById('statusPESERTA').value = this.status;
       
          Session.set('idEditing', this._id);
          $('#modal_peserta').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, PESERTA);
        },

    });


    insertPESERTA = function (tpl) {

       
       let kodeujianPESERTA = tpl.$('input[name="kodeujianPESERTA"]').val();
       
       let kodesiswaPESERTA = tpl.$('input[name="kodesiswaPESERTA"]').val();
       
       let statusPESERTA = tpl.$('input[name="statusPESERTA"]').val();
       

       if(!adaDATA(kodeujianPESERTA) | !adaDATA(kodesiswaPESERTA) | !adaDATA(statusPESERTA) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       PESERTA.insert(
       {
          
       kodeujian: kodeujianPESERTA,
       
       kodesiswa: kodesiswaPESERTA,
       
       status: statusPESERTA,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updatePESERTA = function (tpl) {

        
       let kodeujianPESERTA = tpl.$('input[name="kodeujianPESERTA"]').val();
       
       let kodesiswaPESERTA = tpl.$('input[name="kodesiswaPESERTA"]').val();
       
       let statusPESERTA = tpl.$('input[name="statusPESERTA"]').val();
       

       if(!adaDATA(kodeujianPESERTA) | !adaDATA(kodesiswaPESERTA) | !adaDATA(statusPESERTA) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       PESERTA.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodeujian: kodeujianPESERTA,
       
       kodesiswa: kodesiswaPESERTA,
       
       status: statusPESERTA,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deletePESERTA = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    PESERTA.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
