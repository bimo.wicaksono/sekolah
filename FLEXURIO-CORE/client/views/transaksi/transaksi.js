
        /**
        * Generated from flexurio at Mon Jul  8 11:35:05 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './transaksi.html';

    Template.transaksi.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA TRANSAKSI');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('transaksi', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.transaksi.onRendered(function () {
          ScrollHandler();
      });

      Template.transaksi.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        transaksis: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodeujian: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodekelas: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodepelajaran: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodesoal: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodeguru: { $regex : new RegExp(textSearch, 'i') }},
       
       {tanggal: { $regex : new RegExp(textSearch, 'i') }},
       
       {jam: { $regex : new RegExp(textSearch, 'i') }},
       
       {durasiwaktu: { $regex : new RegExp(textSearch, 'i') }},
       
       {keterangan: { $regex : new RegExp(textSearch, 'i') }},
       
       {status: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return TRANSAKSI.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.transaksi.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deleteTRANSAKSI();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaTRANSAKSI);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_transaksi').modal('show')
       },
       'keyup #namaTRANSAKSI': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updateTRANSAKSI(tpl);
               } else {
                   insertTRANSAKSI(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updateTRANSAKSI(tpl);
            } else {
                insertTRANSAKSI(tpl);
            }
          $('#modal_transaksi').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodeujianTRANSAKSI').value = this.kodeujian;
       
       document.getElementById('kodekelasTRANSAKSI').value = this.kodekelas;
       
       document.getElementById('kodepelajaranTRANSAKSI').value = this.kodepelajaran;
       
       document.getElementById('kodesoalTRANSAKSI').value = this.kodesoal;
       
       document.getElementById('kodeguruTRANSAKSI').value = this.kodeguru;
       
       document.getElementById('tanggalTRANSAKSI').value = this.tanggal;
       
       document.getElementById('jamTRANSAKSI').value = this.jam;
       
       document.getElementById('durasiwaktuTRANSAKSI').value = this.durasiwaktu;
       
       document.getElementById('keteranganTRANSAKSI').value = this.keterangan;
       
       document.getElementById('statusTRANSAKSI').value = this.status;
       
          Session.set('idEditing', this._id);
          $('#modal_transaksi').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, TRANSAKSI);
        },

    });


    insertTRANSAKSI = function (tpl) {

       
       let kodeujianTRANSAKSI = tpl.$('input[name="kodeujianTRANSAKSI"]').val();
       
       let kodekelasTRANSAKSI = tpl.$('input[name="kodekelasTRANSAKSI"]').val();
       
       let kodepelajaranTRANSAKSI = tpl.$('input[name="kodepelajaranTRANSAKSI"]').val();
       
       let kodesoalTRANSAKSI = tpl.$('input[name="kodesoalTRANSAKSI"]').val();
       
       let kodeguruTRANSAKSI = tpl.$('input[name="kodeguruTRANSAKSI"]').val();
       
       let tanggalTRANSAKSI = tpl.$('input[name="tanggalTRANSAKSI"]').val();
       
       let jamTRANSAKSI = tpl.$('input[name="jamTRANSAKSI"]').val();
       
       let durasiwaktuTRANSAKSI = tpl.$('input[name="durasiwaktuTRANSAKSI"]').val();
       
       let keteranganTRANSAKSI = tpl.$('input[name="keteranganTRANSAKSI"]').val();
       
       let statusTRANSAKSI = tpl.$('input[name="statusTRANSAKSI"]').val();
       

       if(!adaDATA(kodeujianTRANSAKSI) | !adaDATA(kodekelasTRANSAKSI) | !adaDATA(kodepelajaranTRANSAKSI) | !adaDATA(kodesoalTRANSAKSI) | !adaDATA(kodeguruTRANSAKSI) | !adaDATA(tanggalTRANSAKSI) | !adaDATA(jamTRANSAKSI) | !adaDATA(durasiwaktuTRANSAKSI) | !adaDATA(keteranganTRANSAKSI) | !adaDATA(statusTRANSAKSI) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       TRANSAKSI.insert(
       {
          
       kodeujian: kodeujianTRANSAKSI,
       
       kodekelas: kodekelasTRANSAKSI,
       
       kodepelajaran: kodepelajaranTRANSAKSI,
       
       kodesoal: kodesoalTRANSAKSI,
       
       kodeguru: kodeguruTRANSAKSI,
       
       tanggal: tanggalTRANSAKSI,
       
       jam: jamTRANSAKSI,
       
       durasiwaktu: durasiwaktuTRANSAKSI,
       
       keterangan: keteranganTRANSAKSI,
       
       status: statusTRANSAKSI,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updateTRANSAKSI = function (tpl) {

        
       let kodeujianTRANSAKSI = tpl.$('input[name="kodeujianTRANSAKSI"]').val();
       
       let kodekelasTRANSAKSI = tpl.$('input[name="kodekelasTRANSAKSI"]').val();
       
       let kodepelajaranTRANSAKSI = tpl.$('input[name="kodepelajaranTRANSAKSI"]').val();
       
       let kodesoalTRANSAKSI = tpl.$('input[name="kodesoalTRANSAKSI"]').val();
       
       let kodeguruTRANSAKSI = tpl.$('input[name="kodeguruTRANSAKSI"]').val();
       
       let tanggalTRANSAKSI = tpl.$('input[name="tanggalTRANSAKSI"]').val();
       
       let jamTRANSAKSI = tpl.$('input[name="jamTRANSAKSI"]').val();
       
       let durasiwaktuTRANSAKSI = tpl.$('input[name="durasiwaktuTRANSAKSI"]').val();
       
       let keteranganTRANSAKSI = tpl.$('input[name="keteranganTRANSAKSI"]').val();
       
       let statusTRANSAKSI = tpl.$('input[name="statusTRANSAKSI"]').val();
       

       if(!adaDATA(kodeujianTRANSAKSI) | !adaDATA(kodekelasTRANSAKSI) | !adaDATA(kodepelajaranTRANSAKSI) | !adaDATA(kodesoalTRANSAKSI) | !adaDATA(kodeguruTRANSAKSI) | !adaDATA(tanggalTRANSAKSI) | !adaDATA(jamTRANSAKSI) | !adaDATA(durasiwaktuTRANSAKSI) | !adaDATA(keteranganTRANSAKSI) | !adaDATA(statusTRANSAKSI) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       TRANSAKSI.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodeujian: kodeujianTRANSAKSI,
       
       kodekelas: kodekelasTRANSAKSI,
       
       kodepelajaran: kodepelajaranTRANSAKSI,
       
       kodesoal: kodesoalTRANSAKSI,
       
       kodeguru: kodeguruTRANSAKSI,
       
       tanggal: tanggalTRANSAKSI,
       
       jam: jamTRANSAKSI,
       
       durasiwaktu: durasiwaktuTRANSAKSI,
       
       keterangan: keteranganTRANSAKSI,
       
       status: statusTRANSAKSI,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deleteTRANSAKSI = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    TRANSAKSI.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
