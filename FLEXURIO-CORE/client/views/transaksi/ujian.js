
        /**
        * Generated from flexurio at Mon Jul  8 11:37:26 WIB 2019
        * By bimofikri at Darwin Bimos-MacBook-Air.local 18.6.0 Darwin Kernel Version 18.6.0: Thu Apr 25 23:16:27 PDT 2019; root:xnu-4903.261.4~2/RELEASE_X86_64 x86_64
        */

    import { Template } from 'meteor/templating';
    import { Session } from 'meteor/session';
    import './ujian.html';

    Template.ujian.created = function () {
       Session.set('limit', 50);
       Session.set('oFILTERS', {});
       Session.set('oOPTIONS', {});
       Session.set('textSearch', '');
       Session.set('namaHeader', 'DATA UJIAN');
       Session.set('dataDelete', '');

       this.autorun(function () {
              subscribtion('ujian', Session.get('oFILTERS'), Session.get('oOPTIONS'), Session.get('limit'));
       });
     };

      Template.ujian.onRendered(function () {
          ScrollHandler();
      });

      Template.ujian.helpers({
        isLockMenu: function () {
          return isLockMenu();
        },

        isActionADD: function () {
          return isAdminActions(Session.get('sURLMenu'), 'ADD');
        },

        isActionEDIT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'EDIT');
        },

        isActionDELETE: function () {
          return isAdminActions(Session.get('sURLMenu'), 'DELETE');
        },

        isActionPRINT: function () {
          return isAdminActions(Session.get('sURLMenu'), 'PRINT');
        },

        sTinggiPopUp: function () {
          return 0.6*($(window).height());
        },
        ujians: function() {
          let textSearch = '';
          if(adaDATA(Session.get('textSearch'))) {
             textSearch = Session.get('textSearch').replace('#', '').trim();
          }

          let oOPTIONS = {
             sort: {createAt: -1},
             limit: Session.get('limit')
          }

          let oFILTERS = {
             aktifYN: 1,
             $or: [
             
       {kodeujian: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodekelas: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodepelajaran: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodesoal: { $regex : new RegExp(textSearch, 'i') }},
       
       {kodeguru: { $regex : new RegExp(textSearch, 'i') }},
       
       {tanggal: { $regex : new RegExp(textSearch, 'i') }},
       
       {jam: { $regex : new RegExp(textSearch, 'i') }},
       
       {durasiwaktu: { $regex : new RegExp(textSearch, 'i') }},
       
       {keterangan: { $regex : new RegExp(textSearch, 'i') }},
       
       {status: { $regex : new RegExp(textSearch, 'i') }},
       
             {_id: { $regex : new RegExp(textSearch, 'i') }},
             ]
          }

          return UJIAN.find(
              oFILTERS,
              oOPTIONS
          );
        }
    });

    Template.ujian.events({
       'click a.cancel': function(e, tpl){
          e.preventDefault();
          Session.set('idEditing', '');
       },

       'click a.deleteDataOK': function(e, tpl){
          e.preventDefault();
          deleteUJIAN();
          FlashMessages.sendWarning('Attention, ' + Session.get('dataDelete') + ' successfully DELETE !');
          $('#modal_formDeleting').modal('hide');
       },
       'click a.deleteData': function(e, tpl){
          e.preventDefault();
          Session.set('dataDelete', Session.get('namaHeader').toLowerCase() + ' ' + this.namaUJIAN);
          Session.set('idDeleting', this._id);
          $('#modal_formDeleting').modal('show');
       },

       'click a.create': function(e, tpl){
          e.preventDefault();
          $('#modal_ujian').modal('show')
       },
       'keyup #namaUJIAN': function (e, tpl) {
           e.preventDefault();
           if (e.keyCode == 13) {
               if (adaDATA(Session.get('idEditing'))) {
                   updateUJIAN(tpl);
               } else {
                   insertUJIAN(tpl);
               }
           }
       },
       'click a.save': function(e, tpl){
            e.preventDefault();
            if (adaDATA(Session.get('idEditing'))) {
                updateUJIAN(tpl);
            } else {
                insertUJIAN(tpl);
            }
          $('#modal_ujian').modal('hide')
       },

       'click a.editData': function(e, tpl){
          e.preventDefault();
          
       document.getElementById('kodeujianUJIAN').value = this.kodeujian;
       
       document.getElementById('kodekelasUJIAN').value = this.kodekelas;
       
       document.getElementById('kodepelajaranUJIAN').value = this.kodepelajaran;
       
       document.getElementById('kodesoalUJIAN').value = this.kodesoal;
       
       document.getElementById('kodeguruUJIAN').value = this.kodeguru;
       
       document.getElementById('tanggalUJIAN').value = this.tanggal;
       
       document.getElementById('jamUJIAN').value = this.jam;
       
       document.getElementById('durasiwaktuUJIAN').value = this.durasiwaktu;
       
       document.getElementById('keteranganUJIAN').value = this.keterangan;
       
       document.getElementById('statusUJIAN').value = this.status;
       
          Session.set('idEditing', this._id);
          $('#modal_ujian').modal('show')
       },

       'submit form.form-comments': function (e, tpl) {
            e.preventDefault();
            flxcomments(e, tpl, UJIAN);
        },

    });


    insertUJIAN = function (tpl) {

       
       let kodeujianUJIAN = tpl.$('input[name="kodeujianUJIAN"]').val();
       
       let kodekelasUJIAN = tpl.$('input[name="kodekelasUJIAN"]').val();
       
       let kodepelajaranUJIAN = tpl.$('input[name="kodepelajaranUJIAN"]').val();
       
       let kodesoalUJIAN = tpl.$('input[name="kodesoalUJIAN"]').val();
       
       let kodeguruUJIAN = tpl.$('input[name="kodeguruUJIAN"]').val();
       
       let tanggalUJIAN = tpl.$('input[name="tanggalUJIAN"]').val();
       
       let jamUJIAN = tpl.$('input[name="jamUJIAN"]').val();
       
       let durasiwaktuUJIAN = tpl.$('input[name="durasiwaktuUJIAN"]').val();
       
       let keteranganUJIAN = tpl.$('input[name="keteranganUJIAN"]').val();
       
       let statusUJIAN = tpl.$('input[name="statusUJIAN"]').val();
       

       if(!adaDATA(kodeujianUJIAN) | !adaDATA(kodekelasUJIAN) | !adaDATA(kodepelajaranUJIAN) | !adaDATA(kodesoalUJIAN) | !adaDATA(kodeguruUJIAN) | !adaDATA(tanggalUJIAN) | !adaDATA(jamUJIAN) | !adaDATA(durasiwaktuUJIAN) | !adaDATA(keteranganUJIAN) | !adaDATA(statusUJIAN) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       UJIAN.insert(
       {
          
       kodeujian: kodeujianUJIAN,
       
       kodekelas: kodekelasUJIAN,
       
       kodepelajaran: kodepelajaranUJIAN,
       
       kodesoal: kodesoalUJIAN,
       
       kodeguru: kodeguruUJIAN,
       
       tanggal: tanggalUJIAN,
       
       jam: jamUJIAN,
       
       durasiwaktu: durasiwaktuUJIAN,
       
       keterangan: keteranganUJIAN,
       
       status: statusUJIAN,
       
          aktifYN: 1,
          createByID: UserID(),
          createBy:UserName(),
          createAt: new Date()
       },
       function (err, id) {
          if(err) {
             FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
          } else {
             Session.set('isCreating', false);
             FlashMessages.sendSuccess('Thanks, your data is successfully saved');
          }
       }
       );
    };


    updateUJIAN = function (tpl) {

        
       let kodeujianUJIAN = tpl.$('input[name="kodeujianUJIAN"]').val();
       
       let kodekelasUJIAN = tpl.$('input[name="kodekelasUJIAN"]').val();
       
       let kodepelajaranUJIAN = tpl.$('input[name="kodepelajaranUJIAN"]').val();
       
       let kodesoalUJIAN = tpl.$('input[name="kodesoalUJIAN"]').val();
       
       let kodeguruUJIAN = tpl.$('input[name="kodeguruUJIAN"]').val();
       
       let tanggalUJIAN = tpl.$('input[name="tanggalUJIAN"]').val();
       
       let jamUJIAN = tpl.$('input[name="jamUJIAN"]').val();
       
       let durasiwaktuUJIAN = tpl.$('input[name="durasiwaktuUJIAN"]').val();
       
       let keteranganUJIAN = tpl.$('input[name="keteranganUJIAN"]').val();
       
       let statusUJIAN = tpl.$('input[name="statusUJIAN"]').val();
       

       if(!adaDATA(kodeujianUJIAN) | !adaDATA(kodekelasUJIAN) | !adaDATA(kodepelajaranUJIAN) | !adaDATA(kodesoalUJIAN) | !adaDATA(kodeguruUJIAN) | !adaDATA(tanggalUJIAN) | !adaDATA(jamUJIAN) | !adaDATA(durasiwaktuUJIAN) | !adaDATA(keteranganUJIAN) | !adaDATA(statusUJIAN) ) {
          FlashMessages.sendWarning('Please complete all of the data to be . . .');
          return;
       }

       UJIAN.update({_id:Session.get('idEditing')},
       { $set:{
          
       kodeujian: kodeujianUJIAN,
       
       kodekelas: kodekelasUJIAN,
       
       kodepelajaran: kodepelajaranUJIAN,
       
       kodesoal: kodesoalUJIAN,
       
       kodeguru: kodeguruUJIAN,
       
       tanggal: tanggalUJIAN,
       
       jam: jamUJIAN,
       
       durasiwaktu: durasiwaktuUJIAN,
       
       keterangan: keteranganUJIAN,
       
       status: statusUJIAN,
       
          updateByID: UserID(),
          updateBy:UserName(),
          updateAt: new Date()
       }
    },
    function (err, id) {
       if(err) {
          FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
       } else {
          Session.set('idEditing', '');
          FlashMessages.sendSuccess('Thanks, your data is successfully saved');
       }
    }
    );
    };

    deleteUJIAN = function () {

    if(!adaDATA(Session.get('idDeleting'))) {
       FlashMessages.sendWarning('Please select data that you want to remove . . .');
       return;
    }

    UJIAN.update({_id:Session.get('idDeleting')},
        { $set:{
           aktifYN: 0,
           deleteByID: UserID(),
           deleteBy:UserName(),
           deleteAt: new Date()
        }
     },
     function (err, id) {
        if(err) {
           FlashMessages.sendWarning('Sorry, Data could not be saved - Please repeat again.');
        } else {
           Session.set('idEditing', '');
           FlashMessages.sendSuccess('Thanks, your data is successfully saved');
        }
     }
     );
    };


    
