/**
 * Flexurio Created by YN.Pamungkas Jayuda on 12/3/15.
 */
import {Mongo} from 'meteor/mongo';

MENU = new Mongo.Collection('menu');
MENUGROUP = new Mongo.Collection('menuGroup');
MENUAUTH = new Mongo.Collection('menuAuth');
MEMBER = Meteor.users;
MESSAGE = new Mongo.Collection('message');
MESSAGEMEMBER = new Mongo.Collection('messageMember');

ACTIVITYLOGS = new Mongo.Collection('activitylogs');
PROFILEDATA = new Mongo.Collection('profileData');
WOTIPE = new Mongo.Collection('woTipe');
WOSUBTIPE = new Mongo.Collection('woSubTipe');
WOSUBTIPEDETAIL = new Mongo.Collection('woSubTipeDetail');
WO = new Mongo.Collection('wo');
APIMANAGER = new Mongo.Collection('apimanager');
KELAS = new Mongo.Collection('kelas'); 
MAPEL = new Mongo.Collection('mapel');
SOAL = new Mongo.Collection('soal'); 
SISWA = new Mongo.Collection('siswa'); 
GURU = new Mongo.Collection('guru'); 
TRANSAKSI = new Mongo.Collection('transaksi'); 
UJIAN = new Mongo.Collection('ujian'); 
PESERTA = new Mongo.Collection('peserta'); 
JAWABAN = new Mongo.Collection('jawaban'); 
